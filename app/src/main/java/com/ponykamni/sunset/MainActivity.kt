package com.ponykamni.sunset

import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {

    private lateinit var sceneView: View
    private lateinit var sunView: View
    private lateinit var sunReflectionView: View
    private lateinit var skyView: View
    private lateinit var seaView: View

    private var state = State.DAY

    private var heightAnimator: ObjectAnimator = ObjectAnimator()
    private var heightReflectionAnimator: ObjectAnimator = ObjectAnimator()
    private var sunsetSkyAnimator: ObjectAnimator = ObjectAnimator()
    private var nightSkyAnimator: ObjectAnimator = ObjectAnimator()

    private val blueSkyColor: Int by lazy {
        ContextCompat.getColor(this, R.color.blue_sky)
    }
    private val sunsetSkyColor: Int by lazy {
        ContextCompat.getColor(this, R.color.sunset_sky)
    }
    private val nightSkyColor: Int by lazy {
        ContextCompat.getColor(this, R.color.night_sky)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        sceneView = findViewById(R.id.scene)
        sunView = findViewById(R.id.sun)
        sunReflectionView = findViewById(R.id.sun_reflection)
        skyView = findViewById(R.id.sky)
        seaView = findViewById(R.id.sea)

        sceneView.setOnClickListener {
            when (state) {
                State.DAY -> startSunsetAnimation()
                State.NIGHT -> startSunriseAnimation()
            }
        }
    }

    enum class State {
        DAY,
        NIGHT
    }

    override fun onStart() {
        super.onStart()

        startExpandAnimator(sunView)
        startExpandAnimator(sunReflectionView)
    }

    private fun startSunsetAnimation() {
        val sunYStart = sunView.top.toFloat()
        val sunYEnd = skyView.height.toFloat()

        val sunReflectionYStart = sunReflectionView.top.toFloat()
        val sunReflectionYEnd = -sunReflectionView.height.toFloat()

        heightAnimator = ObjectAnimator
            .ofFloat(sunView, "y", sunYStart, sunYEnd)
            .setDuration(DURATION)
        heightAnimator.interpolator = AccelerateInterpolator()

        heightReflectionAnimator = ObjectAnimator
            .ofFloat(sunReflectionView, "y", sunReflectionYStart, sunReflectionYEnd)
            .setDuration(DURATION)
        heightReflectionAnimator.interpolator = AccelerateInterpolator()

        sunsetSkyAnimator = ObjectAnimator
            .ofInt(skyView, "backgroundColor", blueSkyColor, sunsetSkyColor)
            .setDuration(DURATION)
        sunsetSkyAnimator.setEvaluator(ArgbEvaluator())

        nightSkyAnimator = ObjectAnimator
            .ofInt(skyView, "backgroundColor", sunsetSkyColor, nightSkyColor)
            .setDuration(DURATION / 2)
        nightSkyAnimator.setEvaluator(ArgbEvaluator())

        val animatorSet = AnimatorSet()
        animatorSet.play(heightAnimator)
            .with(sunsetSkyAnimator)
            .with(heightReflectionAnimator)
            .before(nightSkyAnimator)

        animatorSet.doOnEnd {
            state = State.NIGHT
        }

        animatorSet.start()
    }

    private fun startSunriseAnimation() {
        val sunYEnd = sunView.top.toFloat()
        val sunYStart = skyView.height.toFloat()

        val sunReflectionYEnd = sunReflectionView.top.toFloat()
        val sunReflectionYStart = -sunReflectionView.height.toFloat()

        heightAnimator = ObjectAnimator
            .ofFloat(sunView, "y", sunYStart, sunYEnd)
            .setDuration(DURATION)
        heightAnimator.interpolator = AccelerateInterpolator()

        heightReflectionAnimator = ObjectAnimator
            .ofFloat(sunReflectionView, "y", sunReflectionYStart, sunReflectionYEnd)
            .setDuration(DURATION)
        heightReflectionAnimator.interpolator = AccelerateInterpolator()

        sunsetSkyAnimator = ObjectAnimator
            .ofInt(skyView, "backgroundColor", sunsetSkyColor, blueSkyColor)
            .setDuration(DURATION)
        sunsetSkyAnimator.setEvaluator(ArgbEvaluator())

        nightSkyAnimator = ObjectAnimator
            .ofInt(skyView, "backgroundColor", nightSkyColor, sunsetSkyColor)
            .setDuration(DURATION / 2)
        nightSkyAnimator.setEvaluator(ArgbEvaluator())

        val sunAndSkyAnimatorSet = AnimatorSet()
        sunAndSkyAnimatorSet.play(heightAnimator)
            .with(sunsetSkyAnimator)
            .with(heightReflectionAnimator)

        val animatorSet = AnimatorSet()
        animatorSet.play(nightSkyAnimator)
            .before(sunAndSkyAnimatorSet)

        animatorSet.doOnEnd {
            state = State.DAY
        }

        animatorSet.start()
    }

    private fun startExpandAnimator(view: View) {
        val sunExpandAnimator = ObjectAnimator.ofPropertyValuesHolder(
            view,
            PropertyValuesHolder.ofFloat("scaleX", 0.95f)
            , PropertyValuesHolder.ofFloat("scaleY", 0.95f)
        )
            .setDuration(600)
        sunExpandAnimator.repeatCount = ObjectAnimator.INFINITE
        sunExpandAnimator.repeatMode = ObjectAnimator.REVERSE
        sunExpandAnimator.interpolator = AccelerateDecelerateInterpolator()

        sunExpandAnimator.start()
    }

    companion object {
        private const val DURATION: Long = 3000
    }
}